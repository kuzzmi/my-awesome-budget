import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default function(props, state) {
    return (
        <View style={ styles.component }>
            <Text style={ styles.text }>
                Header Component
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    component: {
    },
    text: {
        fontSize: 16,
    },
});
