import React from 'react';
import { Header } from '.';
import renderer from 'react-test-renderer';

describe('Header', () => {
    it('should be defined', () => {
        const component = renderer.create(
            Header />
        );
        const tree = component.toJSON();
        expect(tree).toBeDefined();
    });
});
