import React from 'react';
import { Text } from '.';
import renderer from 'react-test-renderer';

describe('Text', () => {
    it('should be defined', () => {
        const component = renderer.create(
            Text />
        );
        const tree = component.toJSON();
        expect(tree).toBeDefined();
    });
});
