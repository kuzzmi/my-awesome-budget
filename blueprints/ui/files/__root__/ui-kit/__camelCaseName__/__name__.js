import Renderer from './<%= pascalEntityName %>Renderer';

Renderer.displayName = '<%= pascalEntityName %>';

export default Renderer;
