import React from 'react';
import { <%= pascalEntityName %> } from '.';
import renderer from 'react-test-renderer';

describe('<%= pascalEntityName %>', () => {
    it('should be defined', () => {
        const component = renderer.create(
            <%= pascalEntityName %> />
        );
        const tree = component.toJSON();
        expect(tree).toBeDefined();
    });
});
